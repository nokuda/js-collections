;(function(global) {

    var isUndef = function(obj) {
        return (typeof(obj) === 'undefined');
    }

    var isUndefOrNull = function(obj) {
        return (typeof(obj) === 'undefined' || obj === null);
    }

    var LinkedListNode = function(data, list, before, after) {
        var self = this;
        self.data = data;
        self.list = list;
        self.before = isUndef(before) ? null : before;
        self.after = isUndef(after) ? null : after;
    }

    LinkedListNode.prototype.insertAfter = function(data) {
        var self = this,
            node = new LinkedListNode(data, self.list, self, self.after);

        if (!isUndefOrNull(self.after)) {
            self.after.before = node;
        }

        self.after = node;

        if (self.list.tail == self) {
            self.list.tail = self.after;
        }

        self.list.length++;

        return self.after;
    }

    LinkedListNode.prototype.insertBefore = function(data) {
        var self = this,
            node = new LinkedListNode(data, self.list, self.before, self);

        if (!isUndefOrNull(self.before)) {
            self.before.after = node;
        }

        self.before = node;

        if (self.list.head == self) {
            self.list.head = self.before;
        }

        self.list.length++;

        return self.before;       
    }

    LinkedListNode.prototype.remove = function() {
        var self = this;

        if (!isUndefOrNull(self.before)) {
            self.before.after = self.before;
        }
        else {
            self.list.head = self.after;
        }

        if (!isUndefOrNull(self.after)) {
            self.after.before = self.before;
        }
        else {
            self.list.tail = self.before;
        }

        self.list.length--;

        return self;
    }

    var LinkedList = function(values) {
        var self = this;
        self.head = null;
        self.tail = null;
        self.length = 0;

        if (!isUndefOrNull(values)) {
            var key;
            for (key in values) {
                self.push(values[key]);
            }
        }
    }

    LinkedList.prototype.push = function(data) {
        var self = this;

        if (!isUndefOrNull(self.tail)) {
            self.tail.insertAfter(data);
        }
        else {
            self.tail = new LinkedListNode(data, self);
            self.head = self.tail;
            self.length++;
        }

        return self.length;
    }

    LinkedList.prototype.unshift = function(data) {
        var self = this;

        if (!isUndefOrNull(self.head)) {
            self.head.insertBefore(data);
        }
        else {
            self.head = new LinkedListNode(data, self);
            self.tail = self.head;
            self.length++;
        }

        return self.length;
    }

    LinkedList.prototype.pop = function() {
        var self = this;
        return self.tail.remove().data;
    }

    LinkedList.prototype.shift = function() {
        var self = this;
        return self.head.remove().data;
    }

    LinkedList.prototype.invoke = function() {
        var self = this,
            operation,
            args, 
            current;

        operation = arguments[0];
        args = [];
        if (2 <= arguments.length) {
            args = arguments.slice(1);
        }

        current = self.head;
        if (isUndefOrNull)
        args.unshift(!isUndefOrNull(current) ? current.value : void 0);
        
        while (current != null) {
            operation.apply(self, args);
            current = current.after;
            args[0] = !isUndefOrNull(current) ? current.value : void 0;
        }

        return self;
    }

    LinkedList.prototype.toArray = function() {
        var self = this,
            array = [],
            current = self.head;

        while (!isUndefOrNull(current)) {
            array.push(current.value);
            current = current.after;
        }

        return array;
    };

    LinkedList.prototype.toString = function() {
        var self = this;

        return self.toArray().toString();
    };

    LinkedList.prototype.debug = function() {
        var self = this;
            current, 
            message;

        message = "head: " + self.head;
        current = self.head;
        while (!isUndefOrNull(current)) {
            messeage += current.debug();
            current = current.after;
        }
        
        message += "tail: " + this.tail;
        
        return console.log(message);
    };

    global.LinkedList = LinkedList;

}(this));